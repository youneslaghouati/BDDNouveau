<?php


namespace ccd\Model;


use Illuminate\Database\Eloquent\Model;

class Rating_board  extends Model
{

        protected $table = 'rating_board';
        protected $primaryKey = 'id';
        public $timestamps = false;

}
?>
