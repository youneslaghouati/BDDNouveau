<?php
namespace ccd\models;

class Friends extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'friends';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}