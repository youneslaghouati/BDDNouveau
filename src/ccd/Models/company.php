<?php

namespace ccd\Models;

class Company extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'company';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function games(){
        return $this->belongsToMany('\ccd\Models\Game','game_publishers','game_id','comp_id');
    }


}