<?php
namespace ccd\models;

class Similar_games extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'similar_games';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}