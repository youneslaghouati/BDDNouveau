<?php


namespace ccd\Models;



class Platform extends \Illuminate\Database\Eloquent\Model 
{

        protected $table = 'platform';
        protected $primaryKey = 'id';
        public $timestamps = false;

}
?>
