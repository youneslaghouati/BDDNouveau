<?php

namespace ccd\Models;

class Game extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'game';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	
	public function genres(){
	return $this->belongsToMany('\ccd\Models\Genre','
		game2genre','game_id','genre_id');
	}

	public function similiar_games(){
		return $this->belongsToMay('\ccd\Models\Game','
		similiar_games','game1_id','game2_id');
	}

    public function characters(){
        return $this->belongsToMany('\ccd\Models\Character','game2character','game_id','character_id');
    }
    
    public function rating(){
        return $this->hasOne('\ccd\Models\game2rating');
    }
        
    public function first_appearance_characters(){
        return $this->hasMany('\ccd\Models\Character','first_appeared_in_game_id');
    }
	
	
	public function company(){
        return $this->belongsToMany('\ccd\Models\Company','game_publishers','game_id','comp_id');
    }
    
    public function platforms(){
        return $this->belongsToMany('\ccd\Models\Platform', 'game2platform', 'game_id', 'platform_id');
    }
}