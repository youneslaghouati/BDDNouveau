<?php

namespace ccd\Models;

class Utilisateurs extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'utilisateurs';
	protected $primaryKey = 'email';
	public $timestamps = false;

}