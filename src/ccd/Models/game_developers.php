<?php
namespace ccd\models;

class Game_developers extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'game_developers';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}