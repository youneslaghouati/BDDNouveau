<?php
namespace ccd\controllers;

class Router
{
	private $app;

	public function __construct($app) {
		$this->app = $app;
	}

	public function call($url, $action, $method) {
		return $this->app->$method($url, function () use ($action) {
			$action = explode('@',$action);
			$controllername = '\ccd\controllers\\' . $action[0] . 'Controller';
			$method = $action[1];
			if (class_exists($controllername)) {
				$controller = new $controllername();
			} else {
				$controller = new HomeController();
				$method = 'home';
			}

			call_user_func_array([$controller, $method], func_get_args());
		});
	}
	
	public function get($url, $action) {
		return $this->call($url, $action, 'get');
	}
	
	public function post($url, $action) {
		return $this->call($url, $action, 'post');
	}
	public function any($url, $action) {
		return $this->call($url, $action, 'any');
	}
}