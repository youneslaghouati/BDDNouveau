<?php
namespace ccd\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use ccd\Models\Game;
use ccd\Models\Commentaire;
use ccd\Models\Platform;
use ccd\Models\Game2platform;
class GamesController extends AbstractController
{
    
       
    public function displayone($id){
         $this->app->response()->header('Content-Type', 'application/json');
		$games = Game::where('id',$id)->first();
        $plat=array();
 
        foreach($games->platforms as $p){
            $plat[]=array("Platform"=>array("id"=>$p->id,"name"=>$p->name,"alias"=>$p->alias,"abbreviation"=>$p->abbreviation),"links"=>array("self"=>array("href"=>"/api/platform/".$p->id."/description")));
        }
        echo json_encode(array("game"=>array('id'=>$games->id,"name"=>$games->name,"alias"=>$games->alias,"deck"=>$games->deck,"descripton"=>$games->descr,"original_release_date"=>$games->original_release_date,"Platform"=>$plat,"links"=>array("comments"=>array("href"=>"/api/games/".$id."/comments"),"characters"=>array("href"=>"/api/games/".$id."/characters")))),JSON_UNESCAPED_SLASHES);
    }
    
    public function displaymore(){
         $this->app->response()->header('Content-Type', 'application/json');
        if(isset($_GET['pages'])){
             $games = Game::select('id','name','alias','deck')->skip($_GET['pages']*200)->take(200)->get();
             $pages=$_GET['pages'];
        }else{
             $games = Game::select('id','name','alias','deck')->take(200)->get();

        }
        (isset($_GET['pages'])) ? $pagesuiv='/api/games?pages='.($_GET['pages']+1) : $pagesuiv='/api/games?pages=1';
         (isset($_GET['pages'])) ? ($_GET['pages']!=0) ? $pageprev="/api/games?pages=".($_GET['pages']-1)  :   $pageprev="/api/games?pages=0" : $pageprev="/api/games?pages=1";
        $array_games = array();
        foreach($games as $g){
            $array_games[]=array("game"=>array('id'=>$g->id,"name"=>$g->name,"alias"=>$g->alias,"deck"=>$g->deck),"links"=>array("self"=>array("href"=>"/api/games/$g->id")));
        }
        
        echo json_encode(array('games'=>$array_games,'links'=>array("prev"=>array("href"=>$pageprev),"next"=>array("href"=>$pagesuiv))),JSON_UNESCAPED_SLASHES);
    }
    
    public function comments($id){
         $this->app->response()->header('Content-Type', 'application/json');
        $comm=Commentaire::select('id','titre','contenu','created_at','byuser')->where("togame",$id)->get();
        if($comm->count()==0){
            echo json_encode(array('Aucun commentaires pour ce jeux'));
        }else{
           echo $comm->toJSON(JSON_UNESCAPED_SLASHES);  
        };
    }

    public function Charact($id){
         $this->app->response()->header('Content-Type', 'application/json');
        $games = Game::where('id',$id)->first();
        $plat=array();
 
        foreach($games->characters as $p){
            $plat[]=array($p);
        }
                        echo   json_encode($plat,JSON_UNESCAPED_SLASHES);
    }
 

}