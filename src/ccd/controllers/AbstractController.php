<?php
namespace ccd\controllers;

abstract class AbstractController
{
	protected $app;
	
	function __construct(\Slim\Slim $app = null) {
		if (empty($app)) {
			$app = \Slim\Slim::getInstance();
		}
		$this->app = $app;
	}

	function getUserIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		if(filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}
		return $ip;
	}
    
    function isConnected(){
        return isset($_SESSION['id']); 
    }
	
	/**
	 * $msg (String) : the reason to go back (Unused)
	 */
	function goBack($msg='') {
		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->app->response->redirect($_SERVER['HTTP_REFERER'], 303);
		} else {
			$this->app->response->redirect($this->app->urlFor('home'), 303);
		}
	}
}