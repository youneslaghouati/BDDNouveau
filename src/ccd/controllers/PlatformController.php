<?php
namespace ccd\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use ccd\Models\Game;
use ccd\Models\Commentaire;
use ccd\Models\Platform;
use ccd\Models\Game2platform;
class PlatformController extends AbstractController
{
    
    public function description($id){
         $this->app->response()->header('Content-Type', 'application/json');
            $plat = Platform::where('id',$id)->first();
            echo $plat->toJSON(JSON_UNESCAPED_SLASHES);
    }
 

}