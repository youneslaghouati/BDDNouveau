<?php
require_once 'vendor/autoload.php';

use ccd\Models\Game;
use ccd\Models\Utilisateurs;
use ccd\Models\Commentaire;
use \Illuminate\Database\Capsule\Manager as DB;


$conf = parse_ini_file('src/conf/db.config.ini');
	$db = new DB();
		
	$db->addConnection($conf);
	$db->setAsGlobal();
	$db->bootEloquent();

//generate();
function generate(){
    $faker =  Faker\Factory::create('fr_FR');
    for($i=1;$i<=25000;$i++){
        $users = new Utilisateurs();
        $users->email = $faker->freeEmail;
        $users->adresse = $faker->address;
        $users->télephone = $faker->phoneNumber;
        $users->prenom = $faker->firstName;
        $users->nom = $faker->lastName;
        $users->dateNaissance = $faker->date;
        for($y=1;$y<=10;$y++){
            $com = new Commentaire();
            $com->titre = $faker->sentence;
            $com->contenu= $faker->text;
            $com->byuser=$users->email;
            $com->togame= rand(1,Game::count());
            $com->save();
        }
        $users->save();
    }
    echo 'Generation terminer ...';
}

function question1(){
    $users1 = new Utilisateurs();
    $users1->email = 'didierrrr@gmail.fr';
    $users1->adresse = '4a rue des patates 58965 dzehdzjz';
    $users1->télephone = '06547855412';
    $users1->prenom = 'marcel';
    $users1->nom = 'didier';
    $datenaiss = '23/08/1987';

$date = substr($datenaiss, 6, 4).'-'.substr($datenaiss, 3, 2).'-'.substr($datenaiss, 0, 2);
    $users1->dateNaissance = $date;
    for($y=1;$y<=3;$y++){
        $com = new Commentaire();
        $com->titre = 'Hello titre'+$y;
        $com->contenu= 'contenu '+$y;
        $com->byuser= $users1->email;
        $com->togame= '12342';
        $com->save();
        }
    $users1->save();
    $users2 = new Utilisateurs();
    $users2->email = 'dzazdazrr@gmail.fr';
    $users2->adresse = ' 12 de5 58965 dzehdzjz';
    $users2->télephone = '0548554125';
    $users2->prenom = 'dzzdazd';
    $users2->nom = 'marcel';
    $datenaiss = '20/11/1989';
    $date = substr($datenaiss, 6, 4).'-'.substr($datenaiss, 3, 2).'-'.substr($datenaiss, 0, 2);
    $users2->dateNaissance = $date;
    for($y=1;$y<=3;$y++){
        $com = new Commentaire();
        $com->titre = 'Hello titre'+$y;
        $com->contenu= 'contenu '+$y;
        $com->byuser= $users2->email;
        $com->togame= '12342';
        $com->save();
        }
    $users2->save();

    
}

//infoUser('maurice.emmanuel@yahoo.fr');

function infoUser($user){
    $comments=Commentaire::where('byuser','=',$user)->orderBy('updated_at', 'DESC')->get();
    foreach($comments as $key){
        $titre=$key->titre;
        $contenu=$key->contenu;
        $date=$key->updated_at;
        echo "<br>Titre : $titre<br> Contenu : $contenu<br>update at : $date <br>";
    }
}

//question1();

//requete utilisateurs avec 5 commentaires au moins 
/*
$list = Commentaire::selectRaw('count(byuser), byuser')->groupby('byuser')->having('count(byuser)','>','5')->get();
echo 'Utilisateurs ayant plus de 5 commentaires : ';
foreach($list as $l){
    echo '<br>'.$l->byuser;
};
*/
