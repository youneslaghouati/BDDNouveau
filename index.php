<?php
require_once 'vendor/autoload.php';

use \Illuminate\Database\Capsule\Manager as DB;
use ccd\controllers\Router;



$conf = parse_ini_file('src/conf/db.config.ini');
	$db = new DB();
	$db->addConnection($conf);
	$db->setAsGlobal();
	$db->bootEloquent();

$configuration = [
    'settings' => [
        'displayErrorDetails' => true
    ]
];

$app = new \Slim\Slim($configuration);

//Routeur
$router = new Router($app);

//Création des routes
$router->get('/', 'Home@home')->name("home");
$router->get('/api/games/:id', 'Games@displayone')->name("IDGame");
$router->get('/api/games','Games@displaymore')->name("200Games");
$router->get('/api/games/:id/comments','Games@comments')->name("gamesComment");
$router->get('/api/platform/:id/description','Platform@description')->name("descrplatform");
$router->get('/api/games/:id/characters','Games@Charact')->name("gamesChar");
$app->run();