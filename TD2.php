<?php

require_once  'vendor/autoload.php';

use ccd\Models\Game;
use ccd\Models\Company;
use ccd\Models\Platform;
use \Illuminate\Database\Capsule\Manager as DB;

	$conf = parse_ini_file('src/conf/db.config.ini');
	$db = new DB();
		
	$db->addConnection($conf);
	$db->setAsGlobal();
	$db->bootEloquent();


	
print("QUESTION 1<br>");
foreach(Game::find(12342)->characters as $ch){
    echo $ch->id.'. '.$ch->name.' : '.$ch->deck . "<br>";
    
}

print("<br><br>QUESTION 2<br>");
print "<br>Les jeux dont le nom débute par Mario ayant plus de 3 persos <br>";
foreach(Game::where('name','like','Mario%')->get() as $game){
			echo $game->name . ':'.$game->id."<br>";
        foreach($game->characters as $ch){
				echo'-'.$ch->id.'.'.$ch->name.':'.$ch->deck."<br>";
		}
}

print("<br><br>QUESTION 3<br>");                  
print "<br>Les jeux développés par une compagnie dont le nom contient 'Sony' :<br>";        
foreach(Company::where('name','like','%Sony%')->get() as $compa){
                                echo $compa->name.' : <br>';
                                foreach($compa->games as $c){
                                        echo '-'.$c->name."<br>";
								}
}

print("<br><br>QUESTION 5<br>");
print "\nLes jeux dont le nom débute par Mario ayant plus de 3 persos \n <br>";
foreach(Game::where('name','like','Mario%')
	->has('characters', '>',3)->get() as $game){
			echo $game->name . ':'.$game->id."<br>";
}

print("<br><br>QUESTION 6<br>");
print "\nLes jeux dont le nom débute par Mario ayant un rating commençant par 3+ \n";
foreach( Game::join('game2rating','game2rating.game_id','=','Game.id')->join('game_rating','game_rating.id','=','game2rating.rating_id')->select('Game.name')->where('Game.name','like','Mario%')->where('game_rating.name','like','%3+%')->get() as $game){
	echo $game->name ."<br>";

}



print("<br><br>QUESTION 7<br>");
print "\nLes jeux dont le nom debute par Mario publies par des companies dont le nom contient Inc. & dont le rating initial contient 3+ \n";
foreach(Game::join('game2rating','game2rating.game_id','=','Game.id')->join('game_rating','game_rating.id','=','game2rating.rating_id')->join('game_developers','game_developers.game_id','=','game.id')->join('company','company.id','=','game_developers.comp_id')->select('Game.name')->where('company.name','like','%Inc.%')->where('Game.name','like','Mario%')->where('game_rating.name','like','%3+%')->get() as $game){
			echo "Jeu:" . $game->name."<br>";
}



print("<br><br>QUESTION 8<br>");
print "\nLes jeux dont le nom debute par Mario publies par des companies dont le nom contient Inc. & dont le rating initial contient 3+, et ayant reçu un avis de la part du rating board nommé CERO \n";
foreach(Game::join('game2rating','game2rating.game_id','=','Game.id')->join('game_rating','game_rating.id','=','game2rating.rating_id')->join('game_developers','game_developers.game_id','=','game.id')->join('company','company.id','=','game_developers.comp_id')->join('rating-board','rating_board.id','=','game_rating.rating_board_id')->select('Game.name')->where('company.name','like','%Inc.%')->where('Game.name','like','Mario%')->where('game_rating.name','like','%3+%')->where('rating_board.name','like','CERO')->get() as $game){
			echo "Jeu:" . $game->name."<br>";
}

